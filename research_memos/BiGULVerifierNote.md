1. How to implement Hoare triples?

   1. Should be able to convert to Z3 formulae (hopefully easily).
   2. Expressiveness (as near as possible to Z3).

   
Dec. 11 Report
=================

### Verifier
Users write an incomplete derivation tree for a Hoare triple (ideally only the outermost triple), and the verifier checks its validity. 
  
The verifier infers incomplete parts of the derivation tree by bottom-up synthesizing or top-down inferring (from given triples). 

The verifier may fail to prove the validity when it can't prove a desired triple is implied by triples of its subprograms. Such error may due to:

  1. Subprograms are incorrect and their triples are too weak.
  2. Upper level programs or the program itself are incorrect, they desire a too strong triple on this program.
  3. The logic of the verifier is incomplete.
  
### How much can we omit?

1. A BiGUL logic rule is _inferable_ if given a Hoare triple in the form of its consequence, its required premises can be determined.
  
   All rules except R-Consq is inferable.
   
2. A BiGUL logic rule is _synthesizable_ if it does not require Hoare triples in its premises to be any specific form. (For example R-RearrS requires the triple in its premise to match with `wpat`)

   R-Skip, R-Replace, R-Prod is synthesizable. These rules can always be applied to a corresponding constructor given any Hoare triples of its subprogram (if any). 
   
With inferability and synthesizability, we can safely allow users to omit those parts in a derivation tree:

  1. Consequence rules. For a sequence of applications of R-Consq, its initial triple is known and its final triple can be inferred backward from the next rule (that will not be R-Conseq). We can use z3 to check whether the initial implies the final. 
  2. Synthesizable rules. 

Consequently, we can have a verifier only require the user to write annotations for rearrS/V and Case.

### Verify properties about general Haskell functions. (skip and adaptive)

Incorporate LiquidHaskell? 

(we may require those functions to be a specific simple form in a prototype)
  
  
  
### Debugger 
If a verification error is detected at some point, the user may still feel hard to find the actual erroneous point because at the reported point, its desired triple may be inferred from upper level and triples for its subprograms may be synthesized. The user can't easily decide which one is incorrect.    

Hence we need an interactive debugger to help find the true erroneous point.

A preliminary idea: 
  
  1. Debug upper levels: From a upper level point with a user specified triple, infer required triples for its subprograms, ask the user whether those inferred triples are intended. If not, this is a erroneous point. If yes, continue to the subprogram containing the point the verifier failed. 

  2. Debug lower levels: we have desired triples for subprograms while they are not implied by triples synthesized from subprograms.

  
Dec. 12 Note
===================

1. rearrS is not synthesizable in propositional logic because we have to restrict the precondition to the extent that the inner program preserves the pattern of source after put. To be precise, for `rearrS φ b` with triple `{R} b {R’}`, the most general triple for `rearrS φ b` has a precondition 

```
{s v | s ∈ dom(φ)  ∧  (R φ(s) v)  
       ∧  (∀ s'. R' s' φ(s) v ⇒ s' ∈ range(φ)) }
```

2. For any BiGUL program without recursion, its most general Hoare triple can be obtained by encoding the interpreter of BiGUL into a logical proposition. While such most general Hoare triple is useless for our purpose: simplifying reasoning about BiGUL programs from operational behaviors to specifications.

Dec. 25 Note
=====================

1. rearrS is not synthesizable while it is "invertible", which means for any required triple `{R} rearrS φ b {R'}`, we can derive a "weakest inner triples" for b. It is a stronger (and more useful) property than "inferable" introduced in Dec 11's notes.

2. The def. of "weakest inner triples" is:  
   For a BiGUL constructor `C bᵢ` and a pre-condition and a post-condition  to be established `{P} C bᵢ {P'}`. `{Rᵢ}bᵢ{Rᵢ'}` (for all i) is the weakest inner conditions iff:
   
   i) For any triples `{Tᵢ}bᵢ{Tᵢ'}` that can derive `{P} C bᵢ {P'}`,  `{Tᵢ}bᵢ{Tᵢ'}` implies `{Rᵢ}bᵢ{Rᵢ'}` (it can derive `{Rᵢ}bᵢ{Rᵢ'}` with a consequence rule).
   
   ii) `{Rᵢ}bᵢ{Rᵢ'}` can derive  `{P} C bᵢ {P'}`.

3. The invertible rule for `rearrS` is:

  ```
  invert({P} rearrS φ b {P'}) 
    = { s v | s ∈ range(φ), P φ⁻¹(s) v} 
         b 
      {s' s v | s', s ∈ range(φ), P' φ⁻¹(s') φ⁻¹(s) v }  
    
      , provided that P ⇒ { s v | s ∈ dom(φ) } 
  ```