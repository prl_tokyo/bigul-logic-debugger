{-# LANGUAGE GADTs, ExplicitForAll, ScopedTypeVariables, FlexibleContexts #-}
module Pattern where

import Z3Encode
import Z3.Monad
import Generics.BiGUL
import GHC.InOut
import Data.List
import Data.Maybe
import Control.Monad (liftM2)

-- | Same to BiGUL's Pat, with additional constraints about Z3.
data PatT a env con where
  PVarT   :: Eq a
          => PatT a (Var a) (Maybe a)

  PVarT'  :: PatT a (Var a) (Maybe a)

  PConstT :: (Eq a, Z3ValEncodableIndClass a)
          => a
          -> PatT a () ()

  PProdT  :: Z3TypeEncodableIndClass (a, b)
          => PatT a a' a''
          -> PatT b b' b''
          -> PatT (a, b) (a', b') (a'', b'')

  PLeftT  :: Z3TypeEncodableIndClass (Either a b)
          => PatT a a' a''
          -> PatT (Either a b) a' a''

  PRightT :: Z3TypeEncodableIndClass (Either a b)
          => PatT b b' b''
          -> PatT (Either a b) b' b''

  PInT    :: (InOut a, Z3TypeEncodableIndClass a)
          => PatT (F a) b c
          -> PatT a b c

infixr 1 `PProdT`

patErase :: PatT a env con -> Pat a env con

patErase (PVarT) = PVar
patErase (PVarT') = PVar'
patErase (PConstT a) = PConst a
patErase (PProdT a b) = PProd (patErase a) (patErase b)
patErase (PLeftT a) = PLeft (patErase a)
patErase (PRightT a) = PRight (patErase a)
patErase (PInT a) = PIn (patErase a)


deconstructPatT :: PatT a env con -> a -> Maybe env
deconstructPatT PVarT          x         = return (Var x)
deconstructPatT PVarT'         x         = return (Var x)
deconstructPatT (PConstT c)    x         = if c == x then return () else Nothing
deconstructPatT (PProdT l r)   (x, y)    = liftM2 (,) (deconstructPatT l x)
                                                      (deconstructPatT r y)
deconstructPatT (PLeftT  p)    (Left  x) = deconstructPatT p x
deconstructPatT (PLeftT  _)    _         = Nothing
deconstructPatT (PRightT p)    (Right x) = deconstructPatT p x
deconstructPatT (PRightT _)    _         = Nothing
deconstructPatT (PInT p)       x         = deconstructPatT p (out x)


-- | Same to type Expr in BiGUL, with more constraints on type.
data ExprT env a where

  EDirT   :: Direction env a
          -> ExprT env a

  EConstT :: (Eq a, Z3ValEncodableIndClass a)
          => a  -- constant
          -> ExprT env a

  EProdT  :: Z3TypeEncodableIndClass (a, b)
          => ExprT env a  -- left-hand side expression
          -> ExprT env b  -- right-hand side expression
          -> ExprT env (a, b)

  ELeftT  :: Z3TypeEncodableIndClass (Either a b)
          => ExprT env a
          -> ExprT env (Either a b)

  ERightT :: Z3TypeEncodableIndClass (Either a b)
          => ExprT env b
          -> ExprT env (Either a b)

  EInT    :: (InOut a, Z3TypeEncodableIndClass a) => ExprT env (F a) -> ExprT env a

exprErase :: ExprT env a -> Expr env a

exprErase (EDirT a) = EDir a
exprErase (EConstT a) = EConst a
exprErase (EProdT a b) = EProd (exprErase a) (exprErase b)
exprErase (ELeftT a) = ELeft (exprErase a)
exprErase (ERightT a) = ERight (exprErase a)
exprErase (EInT a) = EIn (exprErase a)

infixr 1 `EProdT`

-- | An unindexed version for Direction, used to to workaround with some typing restrictions
--   on Expr and Pat.
data UnindexedDirection = UDStop | UDLeft UnindexedDirection | UDRight UnindexedDirection
  deriving (Show, Eq, Ord)

eraseIndexDir :: forall env a. Direction env a -> UnindexedDirection
eraseIndexDir (DVar) = UDStop
eraseIndexDir (DLeft i) = UDLeft (eraseIndexDir i)
eraseIndexDir (DRight i) = UDRight (eraseIndexDir i)


-- | encodePat: For a pattern 'pat' of type 't', generate a Z3 proposition asserting the Z3
--   entity 'var' satisfies this pattern.
encodePat :: forall t env con. PatT t env con -> AST -> Z3 AST
encodePat PVarT var = mkTrue

encodePat PVarT' var = mkTrue

encodePat (PConstT c) var = do
  cZ3 <- encodeZ3 c
  mkEq cZ3 var

encodePat (PProdT p1 p2) var = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fst, snd]] <- getDatatypeSortConstructorAccessors sort
  l <- mkApp fst [var]
  r <- mkApp snd [var]
  lProp <- encodePat p1 l
  rProp <- encodePat p2 r
  mkAnd [lProp, rProp]

encodePat (PLeftT l) var = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [isLeft, isRight] <- getDatatypeSortRecognizers sort
  [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sort
  match <- mkApp isLeft [var]
  inner <- mkApp fromLeft [var]
  innerProp <- encodePat l inner
  mkAnd [match, innerProp]

encodePat (PRightT r) var = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [isLeft, isRight] <- getDatatypeSortRecognizers sort
  [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sort
  match <- mkApp isRight [var]
  inner <- mkApp fromRight [var]
  innerProp <- encodePat r inner
  mkAnd [match, innerProp]

encodePat (PInT i) var = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  encodePat i var

-- | Given variable 'v' matching with pattern 'p', return a Z3 expression corresponding
--   to Expr 'e'.
encodeExpr :: forall t a env con. PatT t env con -> AST -> ExprT env a -> Z3 AST

encodeExpr p v (EDirT d) = retriveDir p v d

encodeExpr p v (EConstT c) = encodeZ3 c

encodeExpr p v (EProdT l r) = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable a)
  [mkPair] <- getDatatypeSortConstructors sort
  lExpr <- encodeExpr p v l
  rExpr <- encodeExpr p v r
  mkApp mkPair [lExpr, rExpr]

encodeExpr p v (ELeftT l) = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable a)
  [mkLeft, mkRight] <- getDatatypeSortConstructors sort
  expr <- encodeExpr p v l
  mkApp mkLeft [expr]

encodeExpr p v (ERightT r) = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable a)
  [mkLeft, mkRight] <- getDatatypeSortConstructors sort
  expr <- encodeExpr p v r
  mkApp mkRight [expr]

encodeExpr p v (EInT i) = encodeExpr p v i

-- | retriveDir p v d : for a Z3 entity 'v' matching with pattern 'p', 'd' refers to a variable
--   in pattern 'p' by encoding a path in pattern 'p'. retriveDir returns a Z3 expression equal to
--   that variable in 'v'
retriveDir :: forall env a t con. PatT t env con -> AST -> Direction env a -> Z3 AST

retriveDir PVarT v DVar  = return v

retriveDir PVarT' v DVar = return v

retriveDir (PProdT l r) v (DLeft i) = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fst, snd]] <- getDatatypeSortConstructorAccessors sort
  nV <- mkApp fst [v]
  retriveDir l nV i

retriveDir (PProdT l r) v (DRight i) = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fst, snd]] <- getDatatypeSortConstructorAccessors sort
  nV <- mkApp snd [v]
  retriveDir r nV i

retriveDir (PLeftT i) v d = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sort
  nV <- mkApp fromLeft [v]
  retriveDir i nV d

retriveDir (PRightT i) v d = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sort
  nV <- mkApp fromRight [v]
  retriveDir i nV d

retriveDir (PInT i) v d = retriveDir i v d


-- | encodePatAsExpr pat expr a: For a transformation 'pat -> expr' and a Z3 entity 'a'
--   matching with expr, construct the Z3 entity that is the inverse of 'a' in the transformation.
encodePatAsExpr :: forall a env con a'. PatT a env con -> ExprT env a' -> AST -> Z3 AST
encodePatAsExpr pat expr entity = recur pat id
  where vars :: [(UnindexedDirection, UnindexedDirection)]
        -- each pair (a,b) says a variable a in pat is referred to at b in expr.
        vars = map (\(a,b)->(b,a)) (exprVars expr)

        recur :: forall a1 env1 con1 . PatT a1 env1 con1 -> (UnindexedDirection -> UnindexedDirection) -> Z3 AST

        recur (PVarT) pf =
          let ePathMb = lookup (pf UDStop) vars
              ePath = if ePathMb == Nothing
                         then error "a variable in Pat is not used in Expr. It's not allowed in BiGUL Logic"
                         else fromJust ePathMb
          in retriveUDirInExpr expr entity ePath

        recur (PVarT') pf =
          let ePathMb = lookup (pf UDStop) vars
              ePath = if ePathMb == Nothing
                         then error "a variable in Pat is not used in Expr. It's not allowed in BiGUL Logic"
                         else fromJust ePathMb
          in retriveUDirInExpr expr entity ePath

        recur (PConstT c) pf = encodeZ3 c

        recur (PProdT l r) pf = do
          sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable a1)
          [mkPair] <- getDatatypeSortConstructors sort
          lExpr <- recur l (pf . UDLeft)
          rExpr <- recur r (pf . UDRight)
          mkApp mkPair [lExpr, rExpr]

        recur (PLeftT l) pf = do
          sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable a1)
          [mkLeft, mkRight] <- getDatatypeSortConstructors sort
          expr <- recur l pf
          mkApp mkLeft [expr]

        recur (PRightT r) pf = do
          sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable a1)
          [mkLeft, mkRight] <- getDatatypeSortConstructors sort
          expr <- recur r pf
          mkApp mkRight [expr]

        recur (PInT i) pf = recur i pf

encodeMatchExpr :: forall a env. ExprT env a -> AST -> Z3 AST
encodeMatchExpr expr entity = do
  a1 <- encodeExprAsPat expr entity
  a2 <- encodeMultiOccurrences expr entity
  mkAnd [a1, a2]

-- | encodeMultiOccurrences expr entity: For every variable name appears more than one time in expr,
--   assert all occurences of it match with the same value in 'entity'.
encodeMultiOccurrences :: forall a env. ExprT env a -> AST -> Z3 AST
encodeMultiOccurrences expr entity =
  do as <- sequence (map varMatches vars)
     mkAnd as
  where vars :: [ [ (UnindexedDirection, UnindexedDirection) ] ]      -- each sublist is EDirs in expr refer to the same Dir.
        vars = filter (\x -> length x > 1)
                 $ groupBy (\a b -> (snd a) == (snd b))
                   $ sortBy (\a b -> compare (snd a) (snd b)) (exprVars expr)

        varMatches :: [(UnindexedDirection, UnindexedDirection)] -> Z3 AST
        varMatches (v:vs) = varMatches' vs
          where evM = retriveUDirInExpr expr entity (fst v)
                varMatches' [] = mkTrue
                varMatches' [v1] = do
                  e1 <- retriveUDirInExpr expr entity (fst v1)
                  ev <- evM
                  mkEq ev e1
                varMatches' (v1:v2:vs) = do
                  a1 <- varMatches' (v2:vs)
                  e1 <- retriveUDirInExpr expr entity (fst v1)
                  ev <- evM
                  a2 <- mkEq e1 ev
                  mkAnd [a1, a2]

-- | exprVars expr: extract all variables in expr. Each (d1, d2) in the result means
--   in expr the path d1 leads to an (EDir d2'), and d2 is the unindexed version of d2.
exprVars :: forall a env. ExprT env a -> [(UnindexedDirection, UnindexedDirection)]

exprVars (EDirT d2') = [(UDStop, eraseIndexDir d2')]
exprVars (EConstT _) = []
exprVars (EProdT l r) = map (\(a, b) -> (UDLeft a, b)) (exprVars l)
                          ++ map (\(a, b) -> (UDRight a, b)) (exprVars r)
exprVars (ELeftT i) = exprVars i
exprVars (ERightT i) = exprVars i
exprVars (EInT i) = exprVars i


-- | retriveUDirInExpr: similar to retriveDir, works on Expr and unindexed direction.
retriveUDirInExpr :: forall env t. ExprT env t -> AST -> UnindexedDirection -> Z3 AST

retriveUDirInExpr (EDirT _) v UDStop = return v

retriveUDirInExpr (EProdT l r) v (UDLeft i) = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fst, snd]] <- getDatatypeSortConstructorAccessors sort
  nV <- mkApp fst [v]
  retriveUDirInExpr l nV i

retriveUDirInExpr (EProdT l r) v (UDRight i) = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fst, snd]] <- getDatatypeSortConstructorAccessors sort
  nV <- mkApp snd [v]
  retriveUDirInExpr r nV i

retriveUDirInExpr (ELeftT i) v d = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sort
  nV <- mkApp fromLeft [v]
  retriveUDirInExpr i nV d

retriveUDirInExpr (ERightT i) v d = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sort
  nV <- mkApp fromRight [v]
  retriveUDirInExpr i nV d

retriveUDirInExpr (EInT i) v d = retriveUDirInExpr i v d


-- | Treat expr as a pattern and encode a Z3 assertion on it. It's basically the
--   same to encodePat, while it works on Expr. Note that this function doesn't
--   deal with non-linearity (same variable name occurs in more than one place),
--   which is dealt by encodeMultiOccurrences separately.
--
--   We don't transform an ExprT to PatT and call encodePat due to some difficulty
--   in type indices.
encodeExprAsPat :: forall t env. ExprT env t -> AST -> Z3 AST
encodeExprAsPat (EDirT _) var = mkTrue

encodeExprAsPat (EConstT c) var = do
  cZ3 <- encodeZ3 c
  mkEq cZ3 var

encodeExprAsPat (EProdT p1 p2) var = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [[fst, snd]] <- getDatatypeSortConstructorAccessors sort
  l <- mkApp fst [var]
  r <- mkApp snd [var]
  lProp <- encodeExprAsPat p1 l
  rProp <- encodeExprAsPat p2 r
  mkAnd [lProp, rProp]

encodeExprAsPat (ELeftT l) var = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [isLeft, isRight] <- getDatatypeSortRecognizers sort
  [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sort
  match <- mkApp isLeft [var]
  inner <- mkApp fromLeft [var]
  innerProp <- encodeExprAsPat l inner
  mkAnd [match, innerProp]

encodeExprAsPat (ERightT r) var = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  [isLeft, isRight] <- getDatatypeSortRecognizers sort
  [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sort
  match <- mkApp isRight [var]
  inner <- mkApp fromRight [var]
  innerProp <- encodeExprAsPat r inner
  mkAnd [match, innerProp]

encodeExprAsPat (EInT i) var = do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  encodeExprAsPat i var

