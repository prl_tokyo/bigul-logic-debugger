{-# LANGUAGE GADTs, ExplicitForAll, ScopedTypeVariables #-}
module Z3Encode where

import Z3.Monad
import Data.Bits (finiteBitSize)

data InductiveProperty p a where
  BaseProperty :: p a -> InductiveProperty p a
  ProdProperty :: p (a, b) -> InductiveProperty p a -> InductiveProperty p b -> InductiveProperty p (a, b)
  SumProperty  :: p (Either a b) -> InductiveProperty p a -> InductiveProperty p b -> InductiveProperty p (Either a b)

getProperty :: (InductiveProperty p a) -> p a
getProperty (BaseProperty a) = a
getProperty (ProdProperty a _ _) = a
getProperty (SumProperty a _ _) = a


-- | A type is Z3TypeEncodable if it can be expressed as a Z3 Sort.
data TypeZ3Encodable a = TypeZ3Encodable { sortDef :: (Z3 Sort), sortSymbol :: String }

-- | A type is Z3TypeEncodableInd if it is Z3ValEncodable but all of its sub components are
--   Z3TypeEncodableInd.
type TypeZ3EncodableInd a = InductiveProperty TypeZ3Encodable a

-- | If type t is in Z3TypeEncodableIndClass, then t is Z3TypeEncodableInd. However, GHC cannot
--   deduce sub-components of t is also in Z3TypeEncodableIndClass.
class Z3TypeEncodableIndClass a where
  typeZ3Encoding :: TypeZ3Encodable a
  typeZ3Encoding = getProperty typeZ3EncodingInd

  typeZ3EncodingInd :: InductiveProperty TypeZ3Encodable a
  typeZ3EncodingInd = BaseProperty typeZ3Encoding

instance Z3TypeEncodableIndClass Int where
  typeZ3Encoding = TypeZ3Encodable (mkBvSort (finiteBitSize (0 :: Int)))
                                  ("Bv" ++ show (finiteBitSize (0 :: Int)))

instance Z3TypeEncodableIndClass Integer where
  typeZ3Encoding = TypeZ3Encodable mkIntSort "Int"

instance Z3TypeEncodableIndClass () where
  typeZ3Encoding = TypeZ3Encodable mkUnit "Unit"
    where mkUnit = do sortSym <- mkStringSymbol "Unit"
                      recogSym <- mkStringSymbol "is_Unit"
                      cons <- mkConstructor sortSym recogSym []
                      mkDatatype sortSym [cons]

instance (Z3TypeEncodableIndClass a, Z3TypeEncodableIndClass b) => Z3TypeEncodableIndClass (a, b) where
  typeZ3EncodingInd = ProdProperty p lP rP
    where lP = typeZ3EncodingInd :: InductiveProperty TypeZ3Encodable a
          rP = typeZ3EncodingInd :: InductiveProperty TypeZ3Encodable b
          p =  TypeZ3Encodable encodeType typeName
          aEncoding = getProperty lP
          bEncoding = getProperty rP
          typeName = "Pair_0_" ++ sortSymbol aEncoding ++ "_1__" ++ "__0_" ++ sortSymbol bEncoding ++ "_1"
          encodeType = do
            pairSym <- mkStringSymbol typeName
            isPairSym <- mkStringSymbol ("is_" ++ typeName)
            fstSym <- mkStringSymbol ("fst_" ++ typeName)
            sndSym <- mkStringSymbol ("snd_" ++ typeName)
            lSort <- sortDef aEncoding
            rSort <- sortDef bEncoding
            pairCons <- mkConstructor pairSym isPairSym [(fstSym, Just lSort, 0), (sndSym, Just rSort, 0)]
            mkDatatype pairSym [pairCons]

instance (Z3TypeEncodableIndClass a, Z3TypeEncodableIndClass b) => Z3TypeEncodableIndClass (Either a b) where
  typeZ3EncodingInd = SumProperty p lP rP
    where lP = typeZ3EncodingInd :: InductiveProperty TypeZ3Encodable a
          rP = typeZ3EncodingInd :: InductiveProperty TypeZ3Encodable b
          p =  TypeZ3Encodable encodeType typeName
          aEncoding = getProperty lP
          bEncoding = getProperty rP
          typeName = "Either_0_" ++ sortSymbol aEncoding ++ "_1__" ++ "__0_" ++ sortSymbol bEncoding ++ "_1"
          encodeType = do
            sumSym <- mkStringSymbol typeName

            leftSym <- mkStringSymbol ("left_" ++ typeName)
            rightSym <- mkStringSymbol ("right_" ++ typeName)
            isLeftSym <- mkStringSymbol ("is_left_" ++ typeName)
            isRightSym <- mkStringSymbol ("is_right_" ++ typeName)
            fromLeftSym <- mkStringSymbol ("from_left_" ++ typeName)
            fromRightSym <- mkStringSymbol ("from_right_" ++ typeName)

            lSort <- sortDef aEncoding
            rSort <- sortDef bEncoding

            leftCons <- mkConstructor leftSym isLeftSym [(fromLeftSym, Just lSort, 0)]
            rightCons <- mkConstructor rightSym isRightSym [(fromRightSym, Just rSort, 0)]
            mkDatatype sumSym [leftCons, rightCons]


-- | Z3ValEncodable is a property on type a, saying that there is an encoder from
--   a to Z3 Ast.
newtype Z3ValEncodable a = Z3ValEncodable { z3ValEncoder :: a -> Z3 AST }


-- | Z3ValEncodableIndClass is a property on type a, saying that 'a' is Z3ValEncodable, but
--   also Z3ValEncodableIndClass for sub-components of 'a'.
type Z3TypeEncodableInd a = InductiveProperty Z3ValEncodable a


-- | If type t is in Z3ValEncodableIndClass, then t is Z3ValEncodableInd. However, GHC cannot
--   deduce sub-components of t is also in Z3ValEncodableIndClass.
class Z3ValEncodableIndClass a where
  valEncoderInductive :: InductiveProperty Z3ValEncodable a
  valEncoderInductive = BaseProperty (Z3ValEncodable encodeZ3)

  encodeZ3 :: a -> Z3 AST
  encodeZ3 = z3ValEncoder (getProperty valEncoderInductive)

instance Z3ValEncodableIndClass Integer where
  encodeZ3 n = mkInteger n

instance Z3ValEncodableIndClass Int where
  encodeZ3 n = mkBitvector (finiteBitSize (0 :: Int)) (toInteger n)

instance Z3ValEncodableIndClass () where
  encodeZ3 () = do sort <- mkUnit
                   [c] <- getDatatypeSortConstructors sort
                   mkApp c []
    where mkUnit = do sortSym <- mkStringSymbol "Unit"
                      recogSym <- mkStringSymbol "is_Unit"
                      cons <- mkConstructor sortSym recogSym []
                      mkDatatype sortSym [cons]
