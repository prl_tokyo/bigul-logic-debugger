{-# LANGUAGE GADTs, ExplicitForAll, ScopedTypeVariables #-}
module Proposition where

import Z3.Monad

--
-- This file defines types for pre-conditions and post-conditions. Basically,
-- they are Z3 assertions on Z3 constants representing source, view and new source.
--


-- | PropEnvSV: Z3 entities for a precondition to work on, including the Sorts of
--   the source and view, the Consts representing the source and view.
--
--   Although the construtor does not use s v because the Z3 API is untyped, we still 
--   use source type s and view type v to index the type for safety and convenience.
data PropEnvSV s v = PropEnvSV {- Sort Sort -} AST AST

-- | PropSV: the type the pre-condition.
type PropSV s v = PropEnvSV s v -> Z3 AST


-- | PropEnvSSV: Z3 entities for a post-condition to work on: all entities in a PropEnvSV
--   and the entities for the updated source.
data PropEnvSSV s v = PropEnvSSV {- Sort Sort -} AST AST AST

-- | PropSSV: the type the post-ondition.
type PropSSV s v = PropEnvSSV s v -> Z3 AST


data PrePostConds s v = PrePostConds {preCond :: PropSV s v, postCond :: PropSSV s v}

restrictEnv :: PropEnvSSV s v -> PropEnvSV s v
restrictEnv (PropEnvSSV a b _) = PropEnvSV a b

-- An overloaded interface to access both PropEnvSV and PropEnvSSV.
class PropEnvSVClass a where
  -- sSort :: a -> Sort
  -- vSort :: a -> Sort
  getS  :: a -> AST
  getV  :: a -> AST

class (PropEnvSVClass a) => PropEnvSSVClass a where
  getS' :: a -> AST

instance PropEnvSVClass (PropEnvSSV s v) where
  -- sSort (PropEnvSSV a _ _ _ _) = a
  -- vSort (PropEnvSSV _ a _ _ _) = a
  getS  (PropEnvSSV {-_ _-} a _ _) = a
  getV  (PropEnvSSV {-_ _-} _ a _) = a

instance PropEnvSSVClass (PropEnvSSV s v) where
  getS' (PropEnvSSV {-_ _-} _ _ a) = a

instance PropEnvSVClass (PropEnvSV s v) where
  -- sSort (PropEnvSV a _ _ _) = a
  -- vSort (PropEnvSV _ a _ _) = a
  getS  (PropEnvSV {-_ _-} a _) = a
  getV  (PropEnvSV {-_ _-} _ a) = a
