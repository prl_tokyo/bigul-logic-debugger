module TestZ3 where

import Z3.Monad
import Control.Applicative
import Control.Monad (join)
import Control.Monad.Trans (liftIO)

script2 = evalZ3 $ do
  intSort <- mkIntSort
  sym1 <- mkIntSymbol 1
  sym2 <- mkIntSymbol 2

  c1 <- mkFreshConst "a" intSort
  c2 <- mkFreshConst "a" intSort
  eq <- mkEq c1 c2
  assert =<< mkNot eq
  s <- benchmarkToSMTLibString "" "" "" "" [] eq
  liftIO $ putStrLn s
  check

script = evalZ3 $ do
  sSort <- mkUninterpretedSort =<< (mkStringSymbol "Source")
  vSort <- mkUninterpretedSort =<< (mkStringSymbol "View")

  fstSym <- mkStringSymbol "fst"
  sndSym <- mkStringSymbol "snd"
  sPairSym <- mkStringSymbol "SPair"
  isSPairSym <- mkStringSymbol "isSPair"

  sPairCons <- mkConstructor sPairSym isSPairSym [(fstSym, Just sSort, 0), (sndSym, Just sSort, 0)]
  sPair <- mkDatatype sPairSym [sPairCons]
  [[fst, snd]] <- getDatatypeSortConstructorAccessors sPair

  sourceSym <- mkStringSymbol "source"
  viewSym   <- mkStringSymbol "view"
  newSourceSym <- mkStringSymbol "newSource"

  source <- mkConst sourceSym sPair
  view   <- mkConst viewSym sPair
  newSource <- mkConst newSourceSym sPair

  -- precondtion
  assert =<< join (mkEq <$> (mkApp fst [view]) <*> (mkApp snd [view]))
  assert =<< join (mkEq <$> (mkApp fst [source]) <*> (mkApp snd [source]))


  -- postcondition
  assert =<< mkEq newSource view

  -- consequence
  consequence <- sequence [join (mkEq <$> (mkApp fst [newSource]) <*> (mkApp snd [newSource])),
                           join (mkEq <$> (mkApp fst [source]) <*> (mkApp snd [source])),
                           join (mkEq <$> (mkApp fst [newSource]) <*> (mkApp fst [view])),
                           -- join (mkEq <$> (mkApp fst [source]) <*> (mkApp fst [view])),
                           join (mkEq <$> (mkApp snd [newSource]) <*> (mkApp snd [view])) ]
  assert =<< mkNot =<< mkAnd consequence

  check
