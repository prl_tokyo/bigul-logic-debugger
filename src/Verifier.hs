{-# LANGUAGE GADTs, ExplicitForAll, ScopedTypeVariables, FlexibleContexts #-}
module Verifier where

import Z3Encode
import Triple
import Proposition
import Pattern
import Z3.Monad
import Control.Monad.Trans (liftIO)

-- | If all Case and RearrS are annotated, we can fill in all ommitted triples.
completeHoareTriples :: forall s v. BiGULTriplePartial s v -> BiGULTriple s v

completeHoareTriples (SkipT' NoConds v) = SkipT' (PrePostConds pre post) v
  where pre  = \env ->
                 do v' <- encodeZ3 v
                    mkEq (getV env) v'
        post = \env -> mkEq (getS env) (getS' env)

completeHoareTriples (FailT NoConds s) = FailT (PrePostConds pre post) s
  where pre  = \env -> mkFalse
        post = \env -> mkFalse

completeHoareTriples (ReplaceT NoConds) = ReplaceT (PrePostConds pre post)
  where pre  = \env -> mkTrue
        post = \env -> mkEq (getS' env) (getV env)

completeHoareTriples (ProdT NoConds a b) = ProdT conds left right
  where left  = completeHoareTriples a
        right = completeHoareTriples b
        conds = synthesizeProd (getAnnotation left) (getAnnotation right)

completeHoareTriples (RearrVT NoConds p e b) = RearrVT conds p e b'
  where b' = completeHoareTriples b
        conds = synthesizeRearrV p e (getAnnotation b')

completeHoareTriples (RearrST NoConds _ _ _) = error "RearrST is not synthesized. Please add pre-/post-conditions to them."

completeHoareTriples (CasePatT NoConds _) = error "CasePatT is not synthesized. Please add pre-/post-conditions to them."


-- Cases where annotations are provided by the user, only do recursive call.
completeHoareTriples (FailT (JustConds c) s) = FailT c s

completeHoareTriples (SkipT' (JustConds c) v) = SkipT' c v

completeHoareTriples (ReplaceT (JustConds c)) = ReplaceT c

completeHoareTriples (ProdT (JustConds c) l r) = ProdT c (completeHoareTriples l)
                                                         (completeHoareTriples r)

completeHoareTriples (RearrST (JustConds c) p e i) = RearrST c p e (completeHoareTriples i)

completeHoareTriples (RearrVT (JustConds c) p e i) = RearrVT c p e (completeHoareTriples i)

completeHoareTriples (CasePatT (JustConds c) bs) = CasePatT c (map f bs)
  where f b = case b of
                 NormalT spat vpat i e -> NormalT spat vpat (completeHoareTriples i) e
                 AdaptiveT spat vpat f -> AdaptiveT spat vpat f

synthesizeProd :: forall s s' v v'.
                  (Z3TypeEncodableIndClass (s, s'), Z3TypeEncodableIndClass (v, v'))
               => PrePostConds s v
               -> PrePostConds s' v'
               -> PrePostConds (s, s') (v, v')
synthesizeProd left right = PrePostConds pre post
  where pre  = \env ->
                     do sSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable (s, s'))
                        vSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable (v, v'))
                        [[sFst, sSnd]] <- getDatatypeSortConstructorAccessors sSort --(sSort env)
                        [[vFst, vSnd]] <- getDatatypeSortConstructorAccessors vSort --(vSort env)
                        slSort <- getRange sFst
                        srSort <- getRange sSnd
                        vlSort <- getRange vFst
                        vrSort <- getRange vSnd

                        -- Assert the left components of the view and source satisfy the
                        -- pre-condition of the left BiGUL 'a'.
                        let leftPre = preCond left

                        sL <- mkFreshConstEqualTo "sL" slSort =<< mkApp sFst [getS env]
                        vL <- mkFreshConstEqualTo "vL" vlSort =<< mkApp vFst [getV env]
                        l <- leftPre (PropEnvSV {-slSort vlSort-} sL vL)

                        -- Assert the right components of the view and source satisfy the
                        -- pre-condition of the left BiGUL 'a'.
                        let rightPre = preCond right
                        sR <- mkFreshConstEqualTo "sR" srSort =<< mkApp sSnd [getS env]
                        vR <- mkFreshConstEqualTo "vR" vrSort =<< mkApp vSnd [getV env]
                        r <- rightPre (PropEnvSV {-srSort vrSort-} sR vR)

                        mkAnd [l, r]

        post = \env ->
                     do sSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable (s, s'))
                        vSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable (v, v'))
                        [[sFst, sSnd]] <- getDatatypeSortConstructorAccessors (sSort {- env -})
                        [[vFst, vSnd]] <- getDatatypeSortConstructorAccessors (vSort {- env -})
                        slSort <- getRange sFst
                        srSort <- getRange sSnd
                        vlSort <- getRange vFst
                        vrSort <- getRange vSnd

                        -- Assert the left components of the view and source satisfy the
                        -- pre-condition of the left BiGUL 'a'.
                        let leftPost = postCond (left)

                        sL <- mkFreshConstEqualTo "sL" slSort =<< mkApp sFst [getS env]
                        vL <- mkFreshConstEqualTo "vL" vlSort =<< mkApp vFst [getV env]
                        nsL <- mkFreshConstEqualTo "nsL" slSort =<< mkApp sFst [getS' env]

                        l <- leftPost (PropEnvSSV {-slSort vlSort-} sL vL nsL)

                        -- Assert the right components of the view and source satisfy the
                        -- pre-condition of the left BiGUL 'a'.
                        let rightPost = postCond (right)

                        sR <- mkFreshConstEqualTo "sR" srSort =<< mkApp sSnd [getS env]
                        vR <- mkFreshConstEqualTo "vR" vrSort =<< mkApp vSnd [getV env]
                        nsR <- mkFreshConstEqualTo "nsR" srSort =<< mkApp sSnd [getS' env]

                        r <- rightPost (PropEnvSSV {-srSort vrSort-} sR vR nsR)

                        mkAnd [l, r]

synthesizeRearrV :: forall s v env con v'.
                    PatT v env con
                 -> ExprT env v'
                 -> PrePostConds s v'
                 -> PrePostConds s v
synthesizeRearrV p e (PrePostConds preInner postInner) = PrePostConds pre post
  where pre = \env -> do
          sMatch <- encodePat p (getV env)
          newV <- encodeExpr p (getV env) e
          inner <- preInner (PropEnvSV (getS env) newV)
          mkAnd [sMatch, inner]
        post = \env -> do
          sMatch <- encodePat p (getV env)
          newV <- encodeExpr p (getV env) e
          inner <- postInner (PropEnvSSV (getS env) newV (getS' env))
          mkAnd [sMatch, inner]


data VerifiyResult = Success | Failure String | Unknown
  deriving (Show, Eq)

notSuccess :: VerifiyResult -> Bool
notSuccess (Failure _) = True
notSuccess Unknown = True
notSuccess _ = False

failIfSat :: String -> Result -> VerifiyResult
failIfSat s x =
  case x of
    Sat -> Failure s
    Unsat -> Success
    Undef -> Unknown

-- |
verifyBiGUL :: forall s v. BiGULTriple s v -> Z3 VerifiyResult

verifyBiGUL (FailT p _) = do
  env <- (genPropSVEnv :: Z3 (PropEnvSV s v))
  assert =<< (preCond p env)
  r <- check
  return (failIfSat "Precondtion for Fail is satisfiable." r)

verifyBiGUL (SkipT' p v) =
  let inferred = completeHoareTriples (SkipT' NoConds v)
  in checkConseq (getAnnotation inferred) p

verifyBiGUL (ReplaceT p) =
  let inferred = completeHoareTriples (ReplaceT NoConds)
  in checkConseq (getAnnotation inferred) p

verifyBiGUL (ProdT p l r) =
  let inferred = synthesizeProd (getAnnotation l) (getAnnotation r)
  in do r_ <- verifyBiGUL l
        if notSuccess r_
           then
             return r_           --- TODO: record path in error information.
           else do
             r_ <- verifyBiGUL r
             if notSuccess r_
                then return r_
               else checkConseq inferred p

verifyBiGUL (RearrVT p pat expr inner) =
  let inferred = synthesizeRearrV pat expr (getAnnotation inner)
  in checkConseq inferred p


verifyBiGUL (RearrST p pat expr inner) =
  let weakestInner = weakestInnerRearrS pat expr p
  in do r1 <- local $ do
          preEnv <- genPropSVEnv :: Z3 (PropEnvSV s v)
          -- check precondition of p implies source is in the domain of
          -- the transformation.
          pre <- (preCond p) preEnv
          match <- encodePat pat (getS preEnv)
          assert =<< notImplies pre match
          fmap (failIfSat "Precondition of rearrS doesn't imply the source matches with the pattern") check
        if notSuccess r1
           then return r1
           else checkConseq (getAnnotation inner) weakestInner

verifyBiGUL (CasePatT pCase bs) = checkEach weakestInner
  where normalBs = filter isNormalB bs
        normalActualConds = normalWithActualConds normalBs
        weakestInner = map (\(aM, aE, b) -> (weakestInnerCaseNormal aM aE pCase, b)) normalActualConds
        checkEach [] = return Success
        checkEach ((weakest, b) : bs) = do
          r <- checkConseq (getAnnotation b) weakest
          if notSuccess r
             then return r
             else checkEach bs

-- weakestInnersDebug: return 'weakestInner' for the case of 'Case' of verifyBiGUL for debugging purposes.
weakestInnersDebug pCase bs = weakestInner
  where normalBs = filter isNormalB bs
        normalActualConds = normalWithActualConds normalBs
        weakestInner = map (\(aM, aE, b) -> (weakestInnerCaseNormal aM aE pCase, b)) normalActualConds

-- normalWithActualConds nbs: given a list of normal branches, return the actual main/exit condtion for
-- each branch, i.e., s v match with the pattern and does not match with any previous patterns.
normalWithActualConds :: forall s v env con env' con'.
                         [CaseBranchPatAnnotated s v PrePostConds]
                      -> [(PropEnvSV s v -> Z3 AST, AST -> Z3 AST, BiGULTriple s v)]

normalWithActualConds normalBs = iter [] (const mkTrue) (const mkTrue) normalBs
  where iter result _ _ [] = reverse result
        iter result prevMainNotMatchM prevExitNotMatchM ((NormalT spat vpat innerb exitPat) : bs) =
          iter ((genActualMain prevMainNotMatchM (spat, vpat),
                 genActualExit prevExitNotMatchM exitPat,
                 innerb) : result)
               (genMainNotMatch prevMainNotMatchM (spat, vpat))
               (genExitNotMatch prevExitNotMatchM exitPat)
               bs

        genActualMain prevNotMatchM (spat, vpat) =
          \env -> do prevNotMatch <- prevNotMatchM env
                     sMatch <- encodePat spat (getS env)
                     vMatch <- encodePat vpat (getV env)
                     mkAnd [sMatch, vMatch, prevNotMatch]

        genActualExit prevNotMatchM exitPat =
          \ns -> do prevNotMatch <- prevNotMatchM ns
                    sMatch <- encodePat exitPat ns
                    mkAnd [sMatch, prevNotMatch]

        genMainNotMatch prevNotMatchM (spat, vpat) =
          \env -> do prevNotMatch <- prevNotMatchM env
                     sMatch <- encodePat spat (getS env)
                     vMatch <- encodePat vpat (getV env)
                     notMatch <- mkNot =<< mkAnd [sMatch, vMatch]
                     mkAnd [prevNotMatch, notMatch]

        genExitNotMatch prevNotMatchM exitPat =
          \ns -> do prevNotMatch <- prevNotMatchM ns
                    notMatch <- mkNot =<< encodePat exitPat ns
                    mkAnd [prevNotMatch, notMatch]

weakestInnerCaseNormal :: forall s v env con.
                       (PropEnvSV s v -> Z3 AST)     -- actual main condition
                       -> (AST -> Z3 AST)            -- actual exit condition
                       -> PrePostConds s v           -- the required triple for outter Case
                       -> PrePostConds s v

weakestInnerCaseNormal aMain aExit (PrePostConds preOut postOut) = PrePostConds pre post
  where pre env = do preOut_ <- preOut env
                     aMain_  <- aMain env
                     mkAnd [preOut_, aMain_]

        post env = do postOut_ <- postOut env
                      aMain_ <- aMain (PropEnvSV (getS' env) (getV env))
                      exit_ <- aExit (getS' env)
                      mkAnd [postOut_, aMain_, exit_]


weakestInnerRearrS :: forall s v s' env con .
                   PatT s env con
                   -> ExprT env s'
                   -> PrePostConds s v
                   -> PrePostConds s' v

weakestInnerRearrS pat expr (PrePostConds preOut postOut) = PrePostConds pre post
  where pre = \env -> do
                 sMatch <- encodeMatchExpr expr (getS env)
                 sInv <- encodePatAsExpr pat expr (getS env)
                 a1 <- preOut (PropEnvSV sInv (getV env))
                 mkAnd [sMatch, a1]
        post = \env -> do
                 nSMatch <- encodeMatchExpr expr (getS' env)
                 sMatch  <- encodeMatchExpr expr (getS env)
                 nSInv <- encodePatAsExpr pat expr (getS' env)
                 sInv <- encodePatAsExpr pat expr (getS env)
                 a1 <- postOut (PropEnvSSV sInv (getV env) nSInv)
                 mkAnd [a1, nSMatch, sMatch]

-- | checkConseq p1 p2 : check whether Hoare triple p2 can be derived from p1 by
--   using the consequence rule in BiGUL logic.
checkConseq :: forall s v. (Z3TypeEncodableIndClass s, Z3TypeEncodableIndClass v)
                 => PrePostConds s v -> PrePostConds s v -> Z3 VerifiyResult
checkConseq (PrePostConds pre1 post1) (PrePostConds pre2 post2) =
  do env <- genPropSSVEnv :: Z3 (PropEnvSSV s v)
     let env' = restrictEnv env
     pre1_ <- pre1 env'
     pre2_ <- pre2 env'
     r1 <- local $ do
       assert =<< notImplies pre2_ pre1_
       fmap (failIfSat "New pre-condition does not imply the old one.") check
     if notSuccess r1
        then return r1
        else do
          local $ do
            post1_ <- post1 env
            post2_ <- post2 env
            t1 <- mkAnd [post1_, pre2_]
            assert =<< notImplies t1 post2_
            fmap (failIfSat "New post-condition is not implied by the old post-condition and the new pre-condition.") check


notImplies :: AST -> AST -> Z3 AST
notImplies p c =  do
  imp <- mkImplies p c
  mkNot imp

assertLog :: AST -> Z3 ()
assertLog f = do
  log <- showZ3AST ([], f)
  liftIO $ putStrLn ("\nI assert: \n" ++ log ++ "\n---------------------")
  assert f

-- | mkFreshConstEqualTo: create a new constant and assert it equals to a value.
mkFreshConstEqualTo :: String -> Sort -> AST -> Z3 AST
mkFreshConstEqualTo name sort val = do
  {-
  var <- mkFreshConst name sort
  eq <- mkEq var val
  assertLog eq
  return var
  -}
  return val  {- It looks like Z3 can simplify the expression itself, making defining a new constant unnecessary. -}


-- | For encodable type s and v, define their corresponding Sorts in Z3 and make two
--   constants as the source and the view for pre-conditions to work on.
genPropSVEnv :: forall s v. (Z3TypeEncodableIndClass s, Z3TypeEncodableIndClass v)
                   => Z3 (PropEnvSV s v)
genPropSVEnv = do
  sSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable s)
  vSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable v)
  sConst <- mkFreshConst "source" sSort
  vConst <- mkFreshConst "view" vSort
  return (PropEnvSV {-sSort vSort-} sConst vConst)

-- | For encodable type s and v, define their corresponding Sorts in Z3 and make three
--   constants as the source, the view, the new source for post-conditions to work on.
genPropSSVEnv :: forall s v. (Z3TypeEncodableIndClass s, Z3TypeEncodableIndClass v)
                   => Z3 (PropEnvSSV s v)
genPropSSVEnv = do
  sSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable s)
  vSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable v)
  sConst <- mkFreshConst "source" sSort
  vConst <- mkFreshConst "view" vSort
  nsConst <- mkFreshConst "newSource" sSort
  return (PropEnvSSV {-sSort vSort-} sConst vConst nsConst)

-- Helper functions to dump Z3 formula.
showZ3AST :: ([AST], AST) -> Z3 String
showZ3AST (l, f) = do
  f' <- simplify f
  benchmarkToSMTLibString "" "" "" "" l f'

printZ3AST :: Z3 ([AST], AST) -> IO ()
printZ3AST aM = str >>= putStrLn
  where str = evalZ3 $ do
                (l, f)  <- aM
                showZ3AST (l, f)

printPreCond :: forall s v. (Z3TypeEncodableIndClass s, Z3TypeEncodableIndClass v) => PropSV s v -> IO ()
printPreCond b = printZ3AST $ do
  env <- (genPropSVEnv :: Z3 (PropEnvSV s v))
  f <- b env
  return ([getS env, getV env], f)

printPostCond :: forall s v. (Z3TypeEncodableIndClass s, Z3TypeEncodableIndClass v) => PropSSV s v -> IO ()
printPostCond b = printZ3AST $ do
  env <- (genPropSSVEnv :: Z3 (PropEnvSSV s v))
  f <- b env
  return ([getS env, getV env, getS' env], f)

printPatT :: forall t a b. (Z3TypeEncodableIndClass t) => PatT t a b -> IO ()
printPatT p = printZ3AST $ do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  v <- mkFreshConst "x" sort
  prop <- encodePat p v
  return ([], prop)

printExprT :: forall t env con a. (Z3TypeEncodableIndClass t) => PatT t env con -> ExprT env a -> IO ()
printExprT p e = printZ3AST $ do
  sort <- sortDef (typeZ3Encoding :: TypeZ3Encodable t)
  v <- mkFreshConst "v" sort
  prop <- encodeExpr p v e
  return ([], prop)
