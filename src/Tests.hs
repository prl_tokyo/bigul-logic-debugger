module Tests where

import Pattern
import Verifier
import Proposition
import Triple
import Z3.Monad
import Generics.BiGUL
import Z3Encode
import Control.Monad

-- Test cases
--

b0 = (SkipT' NoConds 0) :: BiGULTriplePartial Integer Integer
b1 = (ReplaceT NoConds) :: BiGULTriplePartial Integer Integer
b2 = ProdT NoConds b1 b1
b3 = ProdT NoConds b0 b0
b4 = ProdT NoConds b3 b1

test1 = printPreCond (preCond (getAnnotation (completeHoareTriples b3)))

test2 = printPreCond (preCond (getAnnotation (completeHoareTriples b4)))

test3 = printPostCond (postCond (getAnnotation (completeHoareTriples b4)))


p1 :: PatT (Either Integer Integer, (Integer, Integer))
           (Var Integer, (Var Integer, Var Integer))
           (Maybe Integer, (Maybe Integer, Maybe Integer))

p1 = PProdT (PLeftT PVarT) (PProdT PVarT PVarT)

p2 :: PatT (Either Integer Integer, (Integer, Integer))
           (Var Integer, ((), Var Integer))
           (Maybe Integer, ((), Maybe Integer))

-- (Left x, (1, y))
p2 = PProdT (PLeftT PVarT) (PProdT (PConstT 1) PVarT)

-- (Left x, (1, y)) -> (x, y)
e1 :: ExprT (Var Integer, ((), Var Integer)) (Integer, Integer)
e1 = EProdT (EDirT (DLeft DVar)) (EDirT (DRight (DRight DVar)))

test4 = printExprT p2 e1

e2 :: ExprT (Var Integer, ((), Var Integer)) (Integer, (Integer, Integer))
e2 = EProdT (EDirT (DLeft DVar))
            (EProdT (EDirT (DRight (DRight DVar)))
                    (EConstT 1))

test5 = printExprT p2 e2


--- test non-linear pattern matching in Expr.
e3 :: ExprT (Var Integer, ((), Var Integer)) (Integer, (Integer, Integer))
e3 = EProdT (EDirT (DLeft DVar))
            (EProdT (EDirT (DRight (DRight DVar)))
                    (EDirT (DLeft DVar)))

test6 = printZ3AST $ do
  iS <- sortDef (typeZ3Encoding :: TypeZ3Encodable (Integer, (Integer, Integer)))
  c <- mkFreshConst "x" iS
  ast <- encodeMultiOccurrences e3 c
  return ([], ast)

e4 :: ExprT (Var Integer, ((), Var Integer)) ((Integer, Integer), (Integer, Integer))
e4 = EProdT (EProdT (EDirT (DLeft DVar))
                    (EDirT (DLeft DVar)))
            (EProdT (EDirT (DRight (DRight DVar)))
                    (EDirT (DLeft DVar)))

test7 = printZ3AST $ do
  iS <- sortDef (typeZ3Encoding :: TypeZ3Encodable ((Integer, Integer), (Integer, Integer)))
  c <- mkFreshConst "x" iS
  ast <- encodeMultiOccurrences e4 c
  return ([], ast)

-- (x, ((), y)) -> ((x, Left x), (y, x))
e5 :: ExprT (Var Integer, ((), Var Integer)) ((Integer, Either Integer Integer), (Integer, Integer))
e5 = EProdT (EProdT (EDirT (DLeft DVar))
                    (ELeftT
                      (EDirT (DLeft DVar))))
            (EProdT (EDirT (DRight (DRight DVar)))
                    (EDirT (DLeft DVar)))

test8 = printZ3AST $ do
  iS <- sortDef (typeZ3Encoding :: TypeZ3Encodable ((Integer, Either Integer Integer), (Integer, Integer)))
  c <- mkFreshConst "x" iS
  ast <- encodeMatchExpr e5 c
  return ([], ast)

--- test encodePatAsExpr. (inverse transformation from expr to pat)

-- (Left x, (1, y))
p3 :: PatT (Either Integer Integer, (Integer, Integer))
           (Var Integer, ((), Var Integer))
           (Maybe Integer, ((), Maybe Integer))
p3 = PProdT (PRightT PVarT) (PProdT (PConstT 1) PVarT)

test9 = printZ3AST $ do
  iS <- sortDef (typeZ3Encoding :: TypeZ3Encodable ((Integer, Either Integer Integer), (Integer, Integer)))
  c <- mkFreshConst "x" iS
  ast <- encodePatAsExpr p3 e5 c
  return ([], ast)


--- test Case
case0 :: PrePostCondsMb (Either () Integer) Integer
      -> BiGULTriplePartial (Either () Integer) Integer
case0 cond = CasePatT cond [ NormalT (PLeftT (PConstT ())) PVarT
                                   (SkipT' NoConds 0)
                                   (PLeftT (PConstT ()))
                           , NormalT (PRightT PVarT) PVarT
                                         (RearrVT NoConds
                                                  PVarT
                                                  (ERightT (EDirT DVar))
                                                  (ReplaceT NoConds))
                                     PVarT
                           ]

post0 :: PropSSV (Either () Integer) Integer
post0 env = mkTrue

post1 :: PropSSV (Either () Integer) Integer
post1 env = do sSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable (Either () Integer))
               [isLeft, isRight] <- getDatatypeSortRecognizers sSort
               [[fromLeft], [fromRight]] <- getDatatypeSortConstructorAccessors sSort
               ns_is_right <- mkApp isRight [getS' env]
               ns_eq_v <- mkEq (getV env) =<< mkApp fromRight [getS' env]
               mkImplies ns_is_right ns_eq_v


pre0 :: PropSV (Either () Integer) Integer
pre0 env = mkTrue

pre1 :: PropSV (Either () Integer) Integer
pre1 env = do sSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable (Either () Integer))
              [isLeft, isRight] <- getDatatypeSortRecognizers sSort
              mkApp isRight [getS env]

pre2 :: PropSV (Either () Integer) Integer
pre2 env = do sSort <- sortDef (typeZ3Encoding :: TypeZ3Encodable (Either () Integer))
              [isLeft, isRight] <- getDatatypeSortRecognizers sSort
              s_is_left <- mkApp isLeft [getS env]
              zero <- mkInteger 0
              v_is_zero <- mkEq (getV env) zero
              mkImplies s_is_left v_is_zero


-- expected: failure
test10 = evalZ3 $ verifyBiGUL (completeHoareTriples (case0 (JustConds (PrePostConds pre0 post0))))

-- expected: success
test11 = evalZ3 $ verifyBiGUL (completeHoareTriples (case0 (JustConds (PrePostConds pre1 post0))))

-- expected: success
test12 = evalZ3 $ verifyBiGUL (completeHoareTriples (case0 (JustConds (PrePostConds pre2 post0))))

-- expected: success
test13 = evalZ3 $ verifyBiGUL (completeHoareTriples (case0 (JustConds (PrePostConds pre2 post1))))


-- print the weakest inner conditions for case
case01 = completeHoareTriples (case0 (JustConds (PrePostConds pre0 post0)))
test14 = forM weakest (\(prepost, _) ->
                          do printPreCond (preCond prepost)
                             printPostCond (postCond prepost)
                             putStrLn "---------------------")
  where weakest = case case01 of
                    (CasePatT p bs) -> weakestInnersDebug p bs
