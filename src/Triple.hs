{-# LANGUAGE GADTs, ExplicitForAll, ScopedTypeVariables, FlexibleContexts #-}
module Triple where

--
-- This file defines the type for BiGUL programs annotated with Hoare triples.
--

import Proposition
import Z3Encode
import Generics.BiGUL
import Pattern
import Data.Maybe


-- | BiGULTriplePartial s v: BiGUL programs partially annotated with pre-conditions
--   and post-conditions.
type BiGULTriplePartial s v = BiGULAnnotated s v PrePostCondsMb
data PrePostCondsMb s v = JustConds (PrePostConds s v) | NoConds


-- | BiGULTriple s v: BiGUL programs annotated with pre-conditions and post-conditions.
type BiGULTriple s v = BiGULAnnotated s v PrePostConds


-- | BiGULAnnotated s v a : a BiGUL program annotated with type 'a s v'.
--   The name of each constructor is the name of the corresponding constructor
--   appended with 'T'.
data BiGULAnnotated s v a where
  FailT    :: (Z3TypeEncodableIndClass s, Z3TypeEncodableIndClass v)
           => a s v
           -> String
           -> BiGULAnnotated s v a


{-
  SkipT    :: (Eq v, Z3ValEncodableIndClass v)
           => a s v
           -> (s -> v)
           -> BiGULAnnotated s v a
-}

  SkipT'   :: (Eq v, Z3ValEncodableIndClass v,
               Z3TypeEncodableIndClass s,
               Z3TypeEncodableIndClass v)                  -- special case of Skip : Skip (const v)
           => a s v
           -> v
           -> BiGULAnnotated s v a

  ReplaceT :: (Z3TypeEncodableIndClass s)
           => a s s
           -> BiGULAnnotated s s a

  ProdT    :: (Show s, Show s', Show v, Show v',
               Z3TypeEncodableIndClass (s, s'),
               Z3TypeEncodableIndClass (v, v'))
           => a (s, s') (v, v')
           -> BiGULAnnotated s v a    -- program for updating the first components
           -> BiGULAnnotated s' v' a  -- program for updating the second components
           -> BiGULAnnotated (s, s') (v, v') a

  RearrST  :: (Show s', Show v, Z3TypeEncodableIndClass s,
               Z3TypeEncodableIndClass v,
               Z3TypeEncodableIndClass s')
           => a s v
           -> PatT s env con  -- pattern for the original source
           -> ExprT env s'    -- expression computing the intermediate source
           -> BiGULAnnotated s' v a    -- program for updating the intermediate source
           -> BiGULAnnotated s  v a

  RearrVT  :: (Show s, Show v', Z3TypeEncodableIndClass s,
               Z3TypeEncodableIndClass v,
               Z3TypeEncodableIndClass v')
           => a s v
           -> PatT v env con  -- pattern for the original view
           -> ExprT env v'    -- expression computing the new view
           -> BiGULAnnotated s v' a     -- remaining program
           -> BiGULAnnotated s v  a

{-
  CaseT    :: a s v
           -> [(s -> v -> Bool, CaseBranchAnnotated s v a)]  -- branches, each of which consists of
                                                             -- a main condition (on both the source and view)
                                                             -- and an inner action
           -> BiGULAnnotated s v a
-}
  CasePatT :: (Z3TypeEncodableIndClass s, Z3TypeEncodableIndClass v)
           => a s v
           -> [CaseBranchPatAnnotated s v a]
                                                             -- branches, each of which consists of
                                                             -- a main condition (on both the source and view)
                                                             -- and an inner action
           -> BiGULAnnotated s v a

infixr 1 `ProdT`

{-
data CaseBranchAnnotated s v a = NormalT (BiGULAnnotated s v a) (s -> Bool)
                               | AdaptiveT (s -> v -> s)
-}
data CaseBranchPatAnnotated s v a = forall env con env' con' env'' con''.
                                      NormalT (PatT s env con)
                                              (PatT v env' con')
                                              (BiGULAnnotated s v a)
                                              (PatT s env'' con'')
                                  | forall env con env' con'.
                                      AdaptiveT (PatT s env con)
                                                (PatT v env' con')
                                                (s -> v -> s)

isNormalB :: CaseBranchPatAnnotated s v a -> Bool
isNormalB (NormalT _ _ _ _) = True
isNormalB _ = False


-- | toBiGUL: erase annotations.
toBiGUL :: BiGULAnnotated s v a -> BiGUL s v
toBiGUL (SkipT' _ v) = Skip (const v)
-- toBiGUL (SkipT _ f) = Skip f
toBiGUL (FailT _  s) = Fail s
toBiGUL (ReplaceT _) = Replace
toBiGUL (ProdT _ a b) = Prod (toBiGUL a) (toBiGUL b)
toBiGUL (RearrST _ a b c) = RearrS (patErase a) (exprErase b) (toBiGUL c)
toBiGUL (RearrVT _ a b c) = RearrV (patErase a) (exprErase b) (toBiGUL c)
toBiGUL (CasePatT _ bs) = Case (map f bs)
  where f b =
          case b of
            NormalT spat vpat b' exit  -> (c spat vpat, Normal (toBiGUL b') (\s' -> isJust (deconstructPatT exit s')))
            AdaptiveT spat vpat adapt  -> (c spat vpat, Adaptive adapt)
          where c spat vpat = \s v -> isJust (deconstructPatT spat s)
                                      && isJust (deconstructPatT vpat v)

-- | getAnnotation
getAnnotation :: BiGULAnnotated s v a -> a s v
getAnnotation (SkipT' a _) = a
-- getAnnotation (SkipT a _) = a
getAnnotation (FailT a _) = a
getAnnotation (ReplaceT a) = a
getAnnotation (ProdT a _ _) = a
getAnnotation (RearrST a _ _ _) = a
getAnnotation (RearrVT a _ _ _) = a
getAnnotation (CasePatT a _) = a


