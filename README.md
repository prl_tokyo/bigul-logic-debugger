# BiVerifier

A verifier for BiGUL logic. Users annotate a BiGUL program with a pre-condition and a post-condition, asserting the program will succeed on an input satisfying the pre-condition and produce an output satisfying the post-condition. The verifier checks the validality of such assertions according to the BiGUL Logic (Ko and Hu 2018).

# References

Hsiang-Shang Ko and Zhenjiang Hu. 2018. An Axiomatic Basis for Bidirectional Programming. Proc. ACM Program. Lang. 2, POPL, Article 41 (January 2018), 29 pages. https://doi.org/10.1145/3158129
